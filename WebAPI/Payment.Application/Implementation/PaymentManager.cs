﻿using Payment.Infrastructure;
using Payment.Model;
using Payment.Repo;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Payment.Application
{
    public class PaymentManager : IPaymentManager
    {
        private IPaymentService paymentService { get; set; }

        public PaymentManager(PaymentDetailContext _Context)
        {
            paymentService = new PaymentService(_Context);
        }

        public IEnumerable<PaymentModel> GetAll()
            => paymentService.FindAllQueryable()
                        .Select(s => new PaymentModel()
                        {
                            CardNumber = s.CardNumber,
                            CardOwnerName = s.CardOwnerName,
                            CVV = s.CVV,
                            ExpirationDate = s.ExpirationDate,
                            PMId = s.PMId
                        })
                        .ToList();

        public bool InsertUpdate(PaymentModel paymentModel)
        {
            try
            {
                paymentService.InsertOrUpdate(
                    new PaymentDetail()
                    {
                        CardNumber = paymentModel.CardNumber,
                        PMId = paymentModel.PMId,
                        ExpirationDate = paymentModel.ExpirationDate,
                        CardOwnerName = paymentModel.CardOwnerName,
                        CVV = paymentModel.CVV
                    });

                return true;
            }
            catch (Exception ex)
            {
                //TODO: Log the exception
                throw; 
            }            
        }

        public PaymentModel GetById(int id)
        {
            PaymentModel model;

            var entity = paymentService.Get(id);

            if (entity != null)
            {
                model = new PaymentModel()
                {
                    CardNumber = entity.CardNumber,
                    CardOwnerName = entity.CardOwnerName,
                    CVV = entity.CVV,
                    ExpirationDate = entity.ExpirationDate,
                    PMId = (int)entity.PMId
                };
            }
            else
            {
                model = new PaymentModel();
            }

            return model;
        }

        public bool Delete(int id)
        {
            try
            {
                var originalPayment = paymentService.Get(id);
                paymentService.Delete(originalPayment);
                return true;
            }
            catch (Exception ex)
            {
                //TODO: Log the exception
                throw;
            }
        }
    }
}
