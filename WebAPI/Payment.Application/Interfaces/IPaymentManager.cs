﻿using Payment.Model;
using System.Collections.Generic;

namespace Payment.Application
{
    public interface IPaymentManager
    {
        IEnumerable<PaymentModel> GetAll();

        bool InsertUpdate(PaymentModel paymentModel);
        
        PaymentModel GetById(int id);

        bool Delete(int id);
    }
}
