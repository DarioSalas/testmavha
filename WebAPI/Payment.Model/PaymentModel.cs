﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Payment.Model
{
    public class PaymentModel
    {
        [Required]
        public int PMId { get; set; }

        [Required]
        public string CardOwnerName { get; set; }

        [Required]
        public string CardNumber { get; set; }

        [Required]
        public string ExpirationDate { get; set; }

        [Required]
        public string CVV { get; set; }
    }
}
