﻿using Payment.Infrastructure;
using System;

namespace Payment.Repo.Builders
{
    public class PaymentBuilder
    {
        private string cardOwnerName;
        private string cardNumber;
        private string expirationDate;
        private string cvv;

        public PaymentBuilder WithAllRequiredFields(string cardOwnerName, string cardNumber, string expirationDate, string cvv)
        {
            this.cardNumber = cardNumber;
            this.cardOwnerName = cardOwnerName;
            this.cvv = cvv;
            this.expirationDate = expirationDate;

            return this;
        }

        public PaymentBuilder WithCardNumber(string cardNumber)
        {
            this.cardNumber = cardNumber;
            return this;
        }

        public PaymentDetail Build()
        {
            var randomCardNumber = new Random();
            var randomCardOwnerName = new Random();
            var randomCvv = new Random();
            var randomExpirationDate = new Random();

            return new PaymentDetail()
            {
                CardNumber = cardNumber ?? $"{randomCardNumber.Next()}",
                CardOwnerName = cardOwnerName ?? $"{randomCardOwnerName.Next()}",
                CVV = cvv ?? $"{randomCvv.Next().ToString().Substring(0, 2)}",
                ExpirationDate = expirationDate ?? $"{randomExpirationDate.Next().ToString().Substring(0, 4)}"
            };
        }
    }
}
