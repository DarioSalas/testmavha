﻿using Microsoft.EntityFrameworkCore;
using Payment.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Payment.Repo
{
    public class GenericDao<TEntity> : IGenericDao<TEntity> where TEntity : BaseEntity
    {
        public DbSet<TEntity> DbSet { get; }
        private PaymentDetailContext _Context;
        
        public GenericDao(PaymentDetailContext context)
        {
            _Context = context;
            DbSet = context.Set<TEntity>();
        }

        public void InsertOrUpdate(TEntity entity, string userId = null)
        {
            _InsertOrUpdate(entity, userId);
        }

        private void _InsertOrUpdate(TEntity entity, string userId)
        {
            PropertyInfo[] poperties = entity.GetType().GetProperties();
            PropertyInfo propertyId = null;

           
            int multiID = 0;
            foreach (PropertyInfo item in poperties)
            {
                if (item.GetCustomAttribute(typeof(KeyAttribute)) != null)
                {
                    propertyId = item;
                    multiID++;
                }
            }

            int id = (int)propertyId.GetMethod.Invoke(entity, null);

            if (id == 0 || multiID > 1)
            {
                DbSet.Add(entity);
            }
            else
            {
                var local = DbSet.Local.FirstOrDefault(x => x.PMId == id);
                _Context.Entry<TEntity>(entity).State = EntityState.Modified;
            }

            Save();
            _Context.Entry<TEntity>(entity).State = EntityState.Detached;
        }

        public void Delete(TEntity entity)
        {
            DbSet.Remove(entity);
        }

        public virtual TEntity Get(params object[] keyValues)
        {
            //dbContext.Database.Log = s => LOG.Debug(s);
            return DbSet.Find(keyValues);
        }

        public virtual IQueryable<TEntity> FindAllQueryable()
        {
            return DbSet.AsQueryable();
        }

        public void Save()
        {
            try
            {
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_Context != null)
                {
                    _Context.Dispose();
                    _Context = null;
                }
            }
        }

       
    }
}
