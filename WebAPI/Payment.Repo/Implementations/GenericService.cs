﻿using Payment.Infrastructure;
using System;
using System.Linq;

namespace Payment.Repo
{
    public class GenericService<TEntity>: IGenericService<TEntity> where TEntity : BaseEntity
    {
        protected GenericDao<TEntity> genericDao;
        
        public GenericService(PaymentDetailContext context)
        {
            genericDao = new GenericDao<TEntity>(context);
        }

        public virtual void InsertOrUpdate(TEntity entity)
        {
            genericDao.InsertOrUpdate(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            genericDao.Delete(entity);
            genericDao.Save();
        }

        public virtual TEntity Get(params object[] keyValues)
        {
            return genericDao.Get(keyValues);
        }

        public virtual IQueryable<TEntity> FindAllQueryable()
        {
            return genericDao.FindAllQueryable();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (genericDao != null)
                {
                    genericDao.Dispose();
                    genericDao = null;
                }
            }
        }
    }
}
