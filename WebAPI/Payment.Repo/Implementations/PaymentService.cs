﻿using Payment.Infrastructure;

namespace Payment.Repo
{
    public class PaymentService : GenericService<PaymentDetail>, IPaymentService
    {
        public PaymentService(PaymentDetailContext context) : base(context)
        {
        }
    }
}
