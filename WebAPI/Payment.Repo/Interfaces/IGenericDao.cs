﻿using System;
using System.Linq;

namespace Payment.Repo
{
    public interface IGenericDao<TEntity>
        : IDisposable
    {
        void InsertOrUpdate(TEntity entity, string userId = null);
        void Delete(TEntity entity);
        TEntity Get(params object[] keyValues);
        IQueryable<TEntity> FindAllQueryable();
    }
}
