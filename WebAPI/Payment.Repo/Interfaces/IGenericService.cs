﻿using Payment.Infrastructure;
using System;
using System.Linq;

namespace Payment.Repo
{
    public interface IGenericService<TEntity> : IDisposable where TEntity : BaseEntity
    {
        void InsertOrUpdate(TEntity entity);
        void Delete(TEntity entity);
        IQueryable<TEntity> FindAllQueryable();
        TEntity Get(params object[] keyValues);
    }
}
