﻿using Payment.Infrastructure;

namespace Payment.Repo
{
    public interface IPaymentService : IGenericService<PaymentDetail>
    {
    }
}
