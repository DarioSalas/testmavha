using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Payment.Infrastructure;
using Payment.Repo;
using Payment.Repo.Builders;

namespace Payment.Test
{
    public class TestsPaymentTest
    {
        private PaymentDetailContext _Context;

        [SetUp]
        public void Setup()
        {
            InitContext();
        }

        [Test]
        public void Payment_Add()
        {
            var result = true;

            var payment = new PaymentBuilder().WithCardNumber("Dario Salas Test").Build();

            var paymentService = new PaymentService(_Context);

            try
            {
                paymentService.InsertOrUpdate(payment);
            }
            catch (System.Exception)
            {
                result = false;
            }
           
            Assert.AreEqual(true, result);
        }

        public void InitContext()
        {
            //TODO: extract in a configuration file
            var builder = new DbContextOptionsBuilder<PaymentDetailContext>()
                .UseSqlServer("Server=DESKTOP-P8VQ5G0\\SQLEXPRESS2017;Database=PaymentDetailDB;Trusted_Connection=True;MultipleActiveResultSets=True;");

            var context = new PaymentDetailContext(builder.Options);

            _Context = context;
        }
    }
}