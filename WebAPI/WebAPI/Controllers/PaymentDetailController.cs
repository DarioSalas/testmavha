﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Payment.Application;
using Payment.Model;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentDetailController : ControllerBase
    {
        private readonly IPaymentManager _paymentManager;

        public PaymentDetailController(IConfiguration configuration, IPaymentManager paymentManager)
        {
            _paymentManager = paymentManager;
        }

        // GET: api/PaymentDetail
        [HttpGet]
        public async Task<IEnumerable<PaymentModel>> GetPaymentDetails()
        {
            return (await Task.FromResult(_paymentManager.GetAll()));
        }

        // PUT: api/PaymentDetail/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPaymentDetail(int id, PaymentModel paymentDetail)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }
            
            if (id != paymentDetail.PMId)
            {
                return BadRequest();
            }
            try
            {
                await Task.FromResult(_paymentManager.InsertUpdate(paymentDetail));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaymentDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    return StatusCode(500);
                }
            }

            return StatusCode(200);
        }

        // GET: api/PaymentDetail/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PaymentModel>> GetPaymentDetail(int id)
        {
            var paymentDetail = await Task.FromResult(_paymentManager.GetById(id));

            if (paymentDetail == null)
            {
                return NotFound();
            }

            return paymentDetail;
        }

        // POST: api/PaymentDetail
        [HttpPost]
        public async Task<IActionResult> PostPaymentDetail(PaymentModel paymentModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (await Task.FromResult(_paymentManager.InsertUpdate(paymentModel)))
            {
                return StatusCode(200);
            }
            else
            {
                return StatusCode(500);
            }
        }

        // DELETE: api/PaymentDetail/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PaymentModel>> DeletePaymentDetail(int id)
        {
            var paymentDetail = await Task.FromResult(_paymentManager.GetById(id));
            if (paymentDetail == null)
            {
                return StatusCode(404);
            }

            await Task.FromResult(_paymentManager.Delete(id));

            return paymentDetail;
        }

        private bool PaymentDetailExists(int id)
        {
            return (_paymentManager.GetById(id) == null ? false : true);
        }
    }
}
